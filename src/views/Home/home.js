import React from 'react';
import ReactDOM from 'react-dom';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import logo from '../../images/dut.png';
import nov from '../../images/nov.png';
import arch from '../../images/arch.png';
import tick from '../../images/tick.png';
import aud from '../../images/aud.png';
//import novedades from './novedades';
import Novedades from '../News/news';
import { Link } from 'react-router-dom';
import './home.css';

// const useStyles = makeStyles({
///
// });

function App() {
    // const classes = useStyles();
    return (
        <div className="App">
            <header className="App-header1">
                <img src={logo} className="App-logo" alt="logo" />
            </header>
            <div className="homeCards" >
                <div>
                    <Card className="card">
                        <Link to="/novedades">
                            <CardActionArea>
                                <CardMedia
                                    component="img"
                                    alt="Contemplative Reptile"
                                    height="140"
                                    image={nov}
                                    title="Contemplative Reptile"
                                />
                                <CardContent className="cardText">
                                    <Typography className="titWid" gutterBottom variant="h5" component="h2">
                                        Novedades
                                </Typography>
                                    <Typography className="subWid" variant="body2" color="textSecondary" component="p">
                                        Todos se enteran de todo en un instante.
                                </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Link>
                    </Card>
                </div>
                <div>
                    <Card className="card">
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="140"
                                image={aud}
                                title="Contemplative Reptile"
                            />
                            <CardContent className="cardText">
                                <Typography className="titWid" gutterBottom variant="h5" component="h2">
                                    Auditorias
                                </Typography>
                                <Typography className="subWid" variant="body2" color="textSecondary" component="p">
                                    Todos se enteran de todo en un instante.
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </div>
            </div>
            <div className="homeCards2" >
                <div>
                    <Card className="card">
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="140"
                                image={tick}
                                title="Contemplative Reptile"
                            />
                            <CardContent className="cardText">
                                <Typography className="titWid" gutterBottom variant="h5" component="h2">
                                    Tickets
                                </Typography>
                                <Typography className="subWid" variant="body2" color="textSecondary" component="p">
                                    Todos se enteran de todo en un instante.
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </div>
                <div>
                    <Card className="card">
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="140"
                                image={arch}
                                title="Contemplative Reptile"
                            />
                            <CardContent className="cardText">
                                <Typography className="titWid" gutterBottom variant="h5" component="h2">
                                    Archivos
                                </Typography>
                                <Typography className="subWid" variant="body2" color="textSecondary" component="p">
                                    La nube, privada, disponible para todos.
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </div>
            </div>
        </div>
    );
}

export default App;
