import React, { Component } from "react";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Add from "@material-ui/icons/Add";
import Button from "../../components/CustomButtons/Button.jsx";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import TextField from "@material-ui/core/TextField";
import { useSpring, animated } from "react-spring";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import FavoriteBorderOutlined from "@material-ui/icons/FavoriteBorderOutlined";
import ChatBubbleOutline from "@material-ui/icons/ChatBubbleOutline";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CssBaseline from "@material-ui/core/CssBaseline";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import logo from "../../images/dut.png";
import dutylogo from "../../images/dutylogo.png";
import addphoto from "../../images/addphoto.png";
import nov1 from "../../images/nov1.jpg";
import check from "../../images/check.png";
import nov2 from "../../images/nov2.jpg";
import "./news.css";
export default class News extends Component {
  constructor(props) {
    super(props);
    this.nov = {
      novedades: [],
      obj: {}
    };
    this.state = {
      open1: false,
      handleClose2: true,
      tit: "",
      sub: "",
      text: "",
      img: "",
      imagePreviewUrl: addphoto,
      modals: false
    };
  }
  componentDidMount() {}
  _pickImage = () => {
    return (
      <div className="picture-container">
        <div className="picnov picture">
          <img
            style={{ height: "100%" }}
            src={this.state.imagePreviewUrl}
            className="picture-src"
            alt="..."
          />
          <input type="file" onChange={e => this.handleImageChange(e)} />
        </div>
      </div>
    );
  };
  handleImageChange = e => {
    e.preventDefault();
    console.log("Eeee", e);
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      console.log("reader1", reader);
      console.log("file2", file);
      this.setState({
        file: file,
        imagePreviewUrl: reader.result
      });
    };
    reader.readAsDataURL(file);
  };
  _listaNueva = () => {
    if (this.nov.novedades.length > 0) {
      // for (let i in this.nov.novedades) {
      return (
        <div>
          {this.nov.novedades.map((item, i) => {
            return (
              <Card style={{ width: "65%", marginBottom: 15 }}>
                <CardMedia
                  className={useStyles.media}
                  image={this.nov.novedades[i].imagen}
                  title="Paella dish"
                >
                  <img
                    src={this.nov.novedades[i].imagen}
                    style={{ width: "100%" }}
                  />
                </CardMedia>
                <CardContent className="textCardNov">
                  <Typography
                    className="titCard"
                    gutterBottom
                    variant="h5"
                    component="h2"
                  >
                    {this.nov.novedades[i].titulo}
                  </Typography>
                  <Typography
                    className="subCardNov"
                    variant="body2"
                    color="textSecondary"
                    component="h4"
                  >
                    {this.nov.novedades[i].subtitulo}
                  </Typography>
                  <Typography
                    variant="txtCardNov"
                    color="textSecondary"
                    component="p"
                  >
                    {this.nov.novedades[i].texto}
                  </Typography>
                </CardContent>
                ​
                <CardActions className="buttomCard">
                  <div className="iconsButtomCard">
                    <IconButton aria-label="add to favorites">
                      <FavoriteBorderOutlined className="iconFav" />
                    </IconButton>
                    <Typography
                      variant="txtCardNov"
                      color="textSecondary"
                      component="p"
                    >
                      Me gusta
                    </Typography>
                    <IconButton aria-label="share">
                      <ChatBubbleOutline className="iconFav" />
                    </IconButton>
                    <Typography
                      variant="txtCardNov"
                      color="textSecondary"
                      component="p"
                    >
                      Comentar
                    </Typography>
                  </div>
                  ​
                  <div className="divDerec">
                    <Typography
                      variant="txtCardNov"
                      color="textSecondary"
                      component="p"
                    >
                      25 Me gusta
                    </Typography>
                  </div>
                </CardActions>
                ​
                <CardContent
                  style={{
                    justifyContent: "flex-start",
                    flexDirection: "row",
                    display: "flex",
                    paddingTop: 0,
                    paddingBottom: 5
                  }}
                >
                  <div>
                    <img src={dutylogo} className="personNov" alt="logo" />
                  </div>
                  <div style={{ width: "100%" }}>
                    <TextField
                      id="standard-full-width"
                      style={{
                        marginTop: 8,
                        marginBottom: 8,
                        width: "100%",
                        fontSize: 14
                      }}
                      placeholder="Comentar"
                      fullWidth
                      margin="normal"
                      InputLabelProps={{
                        shrink: true
                      }}
                    />
                  </div>
                </CardContent>
              </Card>
            );
          })}
        </div>
      );
      // }
    }
  };
  _listaNov = () => {
    return (
      <div className="divContenido">
        <div className="titMod">
          <Typography className="novTit">Novedades</Typography>
          <div className="divbtn3">
            <Button
              className="btn3"
              onClick={() => {
                this.setState({ open1: !this.state.open1 });
              }}
            >
              <Add className="btn3icon" />
              <Typography className="txtb3">CREAR NOVEDAD</Typography>
            </Button>
          </div>
        </div>
        <div className="divCont">
          <img src={dutylogo} className="logonov" alt="logo" />
          <div>
            {this._listaNueva()}
            <Card style={{ width: "65%", marginBottom: 15 }}>
              <CardMedia
                className={useStyles.media}
                image={nov1}
                title="Paella dish"
              >
                <img src={nov1} style={{ width: "100%" }} />
              </CardMedia>
              <CardContent className="textCardNov">
                <Typography
                  className="titCard"
                  gutterBottom
                  variant="h5"
                  component="h2"
                >
                  Comunicar a su organización de manera sencilla y moderna
                </Typography>
                <Typography
                  className="subCardNov"
                  variant="body2"
                  color="textSecondary"
                  component="h4"
                >
                  Logra que todos conozcan las ultimas novedades
                </Typography>
                <Typography
                  variant="txtCardNov"
                  color="textSecondary"
                  component="p"
                >
                  This impressive paella is a perfect party dish and a fun meal
                  to cook together with your guests. Add 1 cup of frozen peas
                  along with the mussels, if you like.
                </Typography>
              </CardContent>
              <CardActions className="buttomCard">
                <div className="iconsButtomCard">
                  <IconButton aria-label="add to favorites">
                    <FavoriteBorderOutlined className="iconFav" />
                  </IconButton>
                  <Typography
                    variant="txtCardNov"
                    color="textSecondary"
                    component="p"
                  >
                    Me gusta
                  </Typography>
                  <IconButton aria-label="share">
                    <ChatBubbleOutline className="iconFav" />
                  </IconButton>
                  <Typography
                    variant="txtCardNov"
                    color="textSecondary"
                    component="p"
                  >
                    Comentar
                  </Typography>
                </div>
                <div className="divDerec">
                  <Typography
                    variant="txtCardNov"
                    color="textSecondary"
                    component="p"
                  >
                    25 Me gusta
                  </Typography>
                </div>
              </CardActions>
              <CardContent
                style={{
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  display: "flex",
                  paddingTop: 0,
                  paddingBottom: 5
                }}
              >
                <div>
                  <img src={dutylogo} className="personNov" alt="logo" />
                </div>
                <div style={{ width: "100%" }}>
                  <TextField
                    id="standard-full-width"
                    style={{
                      marginTop: 8,
                      marginBottom: 8,
                      width: "100%",
                      fontSize: 14
                    }}
                    placeholder="Comentar"
                    fullWidth
                    margin="normal"
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </div>
              </CardContent>
            </Card>
            <Card style={{ width: "65%", marginBottom: 15 }}>
              <CardMedia
                className={useStyles.media}
                image={nov2}
                title="Paella dish"
              >
                <img src={nov2} style={{ width: "100%" }} />
              </CardMedia>
              <CardContent className="textCardNov">
                <Typography
                  className="titCard"
                  gutterBottom
                  variant="h5"
                  component="h2"
                >
                  Comunicar a su organización de manera sencilla y moderna
                </Typography>
                <Typography
                  className="subCardNov"
                  variant="body2"
                  color="textSecondary"
                  component="h4"
                >
                  Logra que todos conozcan las ultimas novedades
                </Typography>
                <Typography
                  variant="txtCardNov"
                  color="textSecondary"
                  component="p"
                >
                  This impressive paella is a perfect party dish and a fun meal
                  to cook together with your guests. Add 1 cup of frozen peas
                  along with the mussels, if you like.
                </Typography>
              </CardContent>
              <CardActions className="buttomCard">
                <div className="iconsButtomCard">
                  <IconButton aria-label="add to favorites">
                    <FavoriteBorderOutlined className="iconFav" />
                  </IconButton>
                  <Typography
                    variant="txtCardNov"
                    color="textSecondary"
                    component="p"
                  >
                    Me gusta
                  </Typography>
                  <IconButton aria-label="share">
                    <ChatBubbleOutline className="iconFav" />
                  </IconButton>
                  <Typography
                    variant="txtCardNov"
                    color="textSecondary"
                    component="p"
                  >
                    Comentar
                  </Typography>
                </div>
                <div className="divDerec">
                  <Typography
                    variant="txtCardNov"
                    color="textSecondary"
                    component="p"
                  >
                    25 Me gusta
                  </Typography>
                </div>
              </CardActions>
              <CardContent
                style={{
                  justifyContent: "flex-start",
                  flexDirection: "row",
                  display: "flex",
                  paddingTop: 0,
                  paddingBottom: 5
                }}
              >
                <div>
                  <img src={dutylogo} className="personNov" alt="logo" />
                </div>
                <div style={{ width: "100%" }}>
                  <TextField
                    id="standard-full-width"
                    style={{
                      marginTop: 8,
                      marginBottom: 8,
                      width: "100%",
                      fontSize: 14
                    }}
                    placeholder="Comentar"
                    fullWidth
                    margin="normal"
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </div>
              </CardContent>
            </Card>
          </div>
        </div>
      </div>
    );
  };
  setModal = prop => {
    this.setState({ open1: prop });
  };
  setModals = prop => {
    this.setState({ modals: prop });
  };
  _newNovedad = () => {
    this.nov.obj.titulo = this.state.tit;
    this.nov.obj.subtitulo = this.state.sub;
    this.nov.obj.texto = this.state.text;
    this.nov.obj.imagen = this.state.imagePreviewUrl;
    console.log("LOG", this.nov.obj);
    this.nov.novedades.unshift(this.nov.obj);
    this.setState(
      { false: false, tit: "", sub: "", text: "", imagePreviewUrl: addphoto },
      () => {
        this.nov.obj = {};
        this.setModal(false);
        this.setState({ modals: !this.state.modals });
      }
    );
    console.log("LOG", this.nov.novedades);
  };
  _modalSuccess = () => {
    return (
      <Modal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={useStyles.modal}
        open={this.state.modals}
        onClose={() => {
          this.setModals(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        {/* <Fade in={this.state.open1} style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}> */}
        <Card
          style={{
            margin: "auto",
            marginTop: 100,
            width: 350,
            height: 300,
            alignItems: "center"
          }}
        >
          <CardContent className="textCardNov"></CardContent>
          <CardMedia
            style={{ height: "50%", width: "50%", margin: "auto" }}
            className="textCardNov"
            image={check}
            title="check"
          ></CardMedia>
          <CardContent className="textCardNov">
            <Typography
              className="titCard"
              style={{ textAlign: "center" }}
              gutterBottom
              variant="h5"
              component="h2"
            >
              Novedad creada con éxito
            </Typography>
            <Typography
              variant="txtCardNov"
              color="textSecondary"
              component="p"
            >
              Has creado una nueva novedad que podrán ver todos los usuarios
            </Typography>
          </CardContent>
        </Card>
        {/* </Fade> */}
      </Modal>
    );
  };
  _modal = () => {
    return (
      <Modal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        className={useStyles.modal}
        open={this.state.open1}
        style={{ overflow: "scroll" }}
        onClose={() => {
          this.setModal(false);
        }}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        {/* <Fade in={this.state.open1} style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}> */}
        <Card className="cardModal" style={{ margin: "auto", marginTop: 30 }}>
          {this._pickImage()}
          <CardContent className="textCardModal">
            <div style={{ width: "100%" }}>
              <TextField
                id="standard-name"
                label="Titulo"
                style={{ width: "100%", marginTop: 0, marginBottom: 0 }}
                className={useStyles.textField}
                value={this.state.tit}
                inputProps={{
                  onChange: e => {
                    this.setState({ tit: e.target.value });
                  },
                  type: "text"
                }}
                margin="normal"
              />
            </div>
            <div style={{ width: "100%" }}>
              <TextField
                id="standard-name"
                label="Subtitulo"
                style={{ width: "100%", marginTop: 0, marginBottom: 0 }}
                className={useStyles.textField}
                value={this.state.sub}
                inputProps={{
                  onChange: e => {
                    this.setState({ sub: e.target.value });
                  },
                  type: "text"
                }}
                margin="normal"
              />
            </div>
            <div style={{ width: "100%" }}>
              <TextField
                id="standard-name"
                label="Escribir texto"
                style={{ width: "100%", marginTop: 0, marginBottom: 0 }}
                className={useStyles.textField}
                value={this.state.text}
                inputProps={{
                  onChange: e => {
                    this.setState({ text: e.target.value });
                  },
                  type: "text"
                }}
                margin="normal"
              />
            </div>
          </CardContent>
          <CardContent className="buttomNewNod">
            <div className="buttonsHed">
              <Button
                className="btn1"
                onClick={() => {
                  console.warn("se cerro el modal");
                  this.setModal(false);
                }}
              >
                <Typography className="btnNewNov1">CANCELAR</Typography>
              </Button>
              <Button
                className="btn2"
                onClick={() => {
                  this._newNovedad();
                }}
              >
                <Typography className="btnNewNov2">ENVIAR</Typography>
              </Button>
            </div>
          </CardContent>
        </Card>
        {/* </Fade> */}
      </Modal>
    );
  };
  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Header />
        <Toolbar />
        <Container className="containercss">
          {this._listaNov()}
          <div style={{ height: "100%" }}>
            {this._modal()}
            {this._modalSuccess()}
          </div>
          {/* <NovedadesCont /> */}
          {/* <ModalNew /> */}
          {/* <SimpleModal /> */}
        </Container>
      </React.Fragment>
    );
  }
}
// class PictureUpload extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       file: null,
//       imagePreviewUrl: addphoto
//     };
//   }
//   handleImageChange = e => {
//     e.preventDefault();
//     console.log('Eeee', e);
//     let reader = new FileReader();
//     let file = e.target.files[0];
//     reader.onloadend = () => {
//       console.log('reader1', reader);
//       console.log('file2', file);
//       this.setState({
//         file: file,
//         imagePreviewUrl: reader.result
//       });
//     };
//     reader.readAsDataURL(file);
//   };
//   handleSubmit = e => {
//     e.preventDefault();
//     // this.state.file is the file/image uploaded
//     // in this function you can save the image (this.state.file) on form submit
//     // you have to call it yourself
//   };
//   render() {
//     return (
//       <div className="picture-container">
//         <div className="picnov picture">
//           <img
//             src={this.state.imagePreviewUrl}
//             className="picture-src"
//             alt="..."
//           />
//           <input type="file" onChange={e => this.handleImageChange(e)} />
//         </div>
//       </div >
//     );
//   }
// }
function Header(props) {
  return (
    <ElevationScroll {...props}>
      <AppBar>
        <Toolbar className="App-header">
          <div>
            <img src={logo} className="App-logo" alt="logo" />
          </div>
          <div className="buttonsHed">
            <Button className="btn1">
              <ArrowBack className="btn1icon" />
              <Typography className="txtb1">Seguir probando</Typography>
            </Button>
            <Button className="btn2">
              <Typography className="txtb2">Probar 14 días gratis</Typography>
            </Button>
          </div>
        </Toolbar>
      </AppBar>
    </ElevationScroll>
  );
}
function ElevationScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined
  });
  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0
  });
}
const Fade = React.forwardRef(function Fade(props, ref) {
  const { in: open, children, onEnter, onExited, ...other } = props;
  const style = useSpring({
    from: { opacity: 0 },
    to: { opacity: open ? 1 : 0 },
    onStart: () => {
      if (open && onEnter) {
        onEnter();
      }
    },
    onRest: () => {
      if (!open && onExited) {
        onExited();
      }
    }
  });
  return (
    <animated.div
      ref={ref}
      style={[
        style,
        { flexDirection: "row", flex: 1, justifyContent: "center" }
      ]}
      {...other}
    >
      {children}
    </animated.div>
  );
});
ElevationScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func
};
const useStyles = makeStyles(theme => ({
  card: {
    width: "65%",
    marginBottom: 15,
    height: 300
  },
  media: {
    height: 300,
    paddingTop: "56.25%" // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  },
  modal: {
    // display: 'flex',
    // alignItems: 'center',
    // justifyContent: 'center',
    // flexDirection: 'row',
    // flex: 1,
    justifyContent: "center",
    overflow: "scroll"
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3)
  },
  textField: {
    width: "100%",
    marginTop: 0,
    fontSize: 15
  }
}));
