import React, {Component} from 'react';
import {
  BrowserRouter as Router,
  Link,
  NavLink,
  Redirect,
  Prompt
} from "react-router-dom";
import Route from "react-router-dom/Route";
import Home from "./views/Home/home";
import Novedades from "./views/News/news";
import Archivos from "./views/News/news";

//aca agregamos las paginas que vamos creando
class App extends Component {
    state = {
        loggedIn: false
    };
    loginHandle = () => {
        this.setState(prevState => ({
            loggedIn: !prevState.loggedIn
        }))
    };

    render() {
        return (
            <Router>
                <div className="App">
                    <Route path="/home" exact strict render={
                        () => {
                            return (<Home/>);
                        }
                    }/>
                    <Route path="/novedades" exact strict render={
                        () => {
                            return (<Novedades/>);
                        }
                    }/>
                    <Route path="/archivos" exact strict render={
                        () => {
                            return (<Archivos/>);
                        }
                    }/>
                </div>
            </Router>
        );
    }
}

export default App;